// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "PongGameModeBase.generated.h"

class APongBall;

/**
 * 
 */
UCLASS()
class PONG_API APongGameModeBase : public AGameMode
{
	GENERATED_BODY()
		
public:
	APongGameModeBase();
	
	FORCEINLINE void SetMyPongBall(APongBall * InPongBall);
	
	void PostLogin(APlayerController* NewPlayer) override;

	AActor* ChoosePlayerStart_Implementation(AController* Player) override;

	void OnScore(bool bLeftPlayer);

	void BeginMatch();

	void ResetMatch(int32 WinnerPlayerId);

	UFUNCTION()
	void SpeedUp();

protected:
	
	TWeakObjectPtr<APongBall> MyPongBall;

	int32 CurrentPlayerID;

	/** play until score */
	UPROPERTY(EditDefaultsOnly, Category = "Pong")
	int32 MaxScore;

	/** time interval in seconds to increase speed */
	UPROPERTY(EditDefaultsOnly, Category = "Pong")
	float SpeedUpTimeInterval;

	/** value to increase speed */
	UPROPERTY(EditDefaultsOnly, Category = "Pong")
	float SpeedUpValue;
	
	FTimerHandle SpeedUpTimerHandle;
};
