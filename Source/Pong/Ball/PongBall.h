// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PongBall.generated.h"


class UProjectileMovementComponent;
class UBoxComponent;

UCLASS()
class PONG_API APongBall : public AActor
{
	GENERATED_BODY()
	
public:	
	APongBall(const FObjectInitializer& ObjectInitializer);

	void Restart();

	void NewThrow(bool bForLeftPlayer);

	FORCEINLINE void AddSpeed(float InSpeed){ Speed += InSpeed; }

protected:
	void PostInitializeComponents() override;

	void BeginPlay() override;

	void Tick(float DeltaSeconds) override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	void OnImpact(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION(Reliable, NetMulticast)
	void Client_UpdateLocation(const FVector_NetQuantize &NewLoc, bool bSweep = true);
	
	UFUNCTION(Reliable, NetMulticast)
	void Client_PlayHitCosmetics();

protected:

	/** cached pointer to GameMode */
	TWeakObjectPtr<class APongGameModeBase> MyGameMode;

	/** cached pointer to GameState */
	TWeakObjectPtr<class APongGameState> MyGameState;
		
	/** collisions */
	UPROPERTY(VisibleAnywhere, Category = "Pong")
	UBoxComponent* CollisionComponent;
	
	UPROPERTY(EditDefaultsOnly, Category = "Pong")
	float InitialSpeed;

	UPROPERTY(Replicated)
	float Speed;
	
	UPROPERTY(Transient, Replicated)
	FVector Velocity;

	/** hit sound */
	UPROPERTY(EditDefaultsOnly, Category= "Pong")
	class USoundCue* HitSound;
};
