// Fill out your copyright notice in the Description page of Project Settings.

#include "PongBall.h"
#include "Player/PongPaddle.h"
#include "PongGameModeBase.h"
#include "PongGameState.h"

#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UnrealNetwork.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Classes/Sound/SoundCue.h"
#include "UObject/Object.h"

// Sets default values
APongBall::APongBall(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{	
	CollisionComponent = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("CollisionComponent"));
	CollisionComponent->InitBoxExtent(FVector::ForwardVector * 5.0f);
	CollisionComponent->AlwaysLoadOnClient = true;
	CollisionComponent->AlwaysLoadOnServer = true;
	CollisionComponent->bTraceComplexOnMove = true;
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	CollisionComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	CollisionComponent->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
	RootComponent = CollisionComponent;

	Velocity = FVector::ZeroVector;
	InitialSpeed = 250.0f;
	Speed = 250.0f;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	bReplicates = true;
	bReplicateMovement = true;
}

void APongBall::Restart()
{
	Super::Reset();
	
	Speed = InitialSpeed;
	Velocity = FVector::ZeroVector;
	Client_UpdateLocation(FVector::ZeroVector, false);
}

// Called when the game starts or when spawned
void APongBall::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CollisionComponent->OnComponentHit.AddDynamic(this, &APongBall::OnImpact);
}

void APongBall::BeginPlay()
{
	Super::BeginPlay();
		
	if (HasAuthority())
	{
		Restart();

		MyGameMode = Cast<APongGameModeBase>(GetWorld()->GetAuthGameMode());
		MyGameMode->SetMyPongBall(this);
	}

	MyGameState = GetWorld()->GetGameState<APongGameState>();
}

void APongBall::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (HasAuthority())
	{
		if (GetActorLocation().Y > MyGameState->GetPlayableAreaTopRight().Y)
		{
			MyGameMode->OnScore(true);
		}
		else if (GetActorLocation().Y < MyGameState->GetPlayableAreaLowerLeft().Y)
		{
			MyGameMode->OnScore(false);
		}

		Client_UpdateLocation(GetActorLocation() + Velocity * Speed);
	}
}

void APongBall::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(APongBall, Velocity);
}

void APongBall::OnImpact(
	UPrimitiveComponent* HitComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	FVector NormalImpulse, 
	const FHitResult& Hit)
{
	if (HasAuthority())
	{
		if (APongPaddle * Paddle = Cast<APongPaddle>(OtherActor))
		{
			const float XPaddleScale = Paddle->GetPaddleMeshComponent()->GetComponentScale().X;
			const float XPos = (Hit.ImpactPoint - Paddle->GetActorLocation()).X + XPaddleScale * 0.5f;
			float ReflectAngle = ((XPos * Paddle->GetMaxReflectionAngle() * 2.0f) / XPaddleScale) - Paddle->GetMaxReflectionAngle();
			ReflectAngle *= Paddle->GetActorLocation().Y < 0.0f ? -1.0f : 1.0f;
			const FVector ReflectionVector = FVector::RightVector * (Paddle->GetActorLocation().Y < 0.0f ? 1.0f : -1.0f);

			Velocity = FQuat(FVector::UpVector, FMath::DegreesToRadians(ReflectAngle)).RotateVector(ReflectionVector);
		}
		else
		{
			Velocity = FMath::GetReflectionVector(Velocity, Hit.ImpactNormal);
		}

		Client_PlayHitCosmetics();
	}
}

void APongBall::Client_PlayHitCosmetics_Implementation()
{
	if(GetNetMode() != NM_DedicatedServer && HitSound != nullptr)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), HitSound);
	}
}

void APongBall::NewThrow(bool bForLeftPlayer)
{
	const float XStart = FMath::FRandRange(MyGameState->GetPlayableAreaLowerLeft().X,
		MyGameState->GetPlayableAreaTopRight().X);

	const float YStart = bForLeftPlayer ? MyGameState->GetPlayableAreaLowerLeft().Y : MyGameState->GetPlayableAreaTopRight().Y;

	Velocity = FVector(XStart, YStart, 0.0f).GetSafeNormal();

	Client_UpdateLocation(FVector::ZeroVector, false);
}

void APongBall::Client_UpdateLocation_Implementation(const FVector_NetQuantize &NewLoc, bool bSweep)
{
	SetActorLocation(NewLoc, bSweep);
}
