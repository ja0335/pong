// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#include "PongGameModeBase.h"
#include "Player/PongPlayerController.h"
#include "Player/PongPaddle.h"
#include "PongGameState.h"
#include "PongPlayerState.h"
#include "UI/PongWidgetHUD.h"
#include "Ball/PongBall.h"

#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"


APongGameModeBase::APongGameModeBase()
{
	CurrentPlayerID = 0;
	MaxScore = 10;
	SpeedUpTimeInterval = 30.0f;
	SpeedUpValue = 100.0f;
}

void APongGameModeBase::SetMyPongBall(APongBall* InPongBall)
{
	MyPongBall = InPongBall;
}

void APongGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	if (APongPlayerState* PongPlayerState = Cast<APongPlayerState>(NewPlayer->PlayerState))
	{
		PongPlayerState->SetPlayerScoreID(CurrentPlayerID++);
	}
}

AActor* APongGameModeBase::ChoosePlayerStart_Implementation(AController* Player)
{
	AActor* ChoosenPlayerStart = Super::ChoosePlayerStart_Implementation(Player);

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), FoundActors);

	for(AActor* FoundActor : FoundActors)
	{
		if(APlayerStart * PlayerStart = Cast<APlayerStart>(FoundActor))
		{
			if (PlayerStart->PlayerStartTag != TEXT("Taken"))
			{
				PlayerStart->PlayerStartTag = TEXT("Taken");
				ChoosenPlayerStart = FoundActor;

				if (APongPlayerController * PlayerController = Cast<APongPlayerController>(Player))
				{
					PlayerController->SetIsLeftPLayer(PlayerStart->GetActorLocation().X < 0.0f);
				}
				break;
			}
		}
	}

	return ChoosenPlayerStart;
}

void APongGameModeBase::OnScore(bool bLeftPlayer)
{
	int32 WinnerPlayerId = -1;

	for(APlayerState * PlayerState : GameState->PlayerArray)
	{
		if (APongPlayerState * PongPlayerState = Cast<APongPlayerState>(PlayerState))
		{
			if (APongPlayerController * Controller = Cast<APongPlayerController>(PongPlayerState->GetOwner()))
			{
				if (APongPaddle * PongPaddle = Cast<APongPaddle>(Controller->GetPawn()))
				{
					if (bLeftPlayer && Controller->GetPawn()->GetActorLocation().Y < 0.0f)
					{
						PongPlayerState->AddScore();

						if (PongPlayerState->Score == MaxScore)
						{
							MyPongBall->Restart();
							WinnerPlayerId = PongPlayerState->PlayerId;
						}
						else
							MyPongBall->NewThrow(true);
					}
					else if (!bLeftPlayer && Controller->GetPawn()->GetActorLocation().Y > 0.0f)
					{
						PongPlayerState->AddScore();

						if (PongPlayerState->Score == MaxScore)
						{
							MyPongBall->Restart();
							WinnerPlayerId = PongPlayerState->PlayerId;
						}
						else
							MyPongBall->NewThrow(false);
					}
				}
			}		

		}
	}

	if (WinnerPlayerId != -1)
	{
		ResetMatch(WinnerPlayerId);
	}
}

void APongGameModeBase::BeginMatch()
{
	MyPongBall->NewThrow(FMath::RandBool());

	for (APlayerState * PlayerState : GameState->PlayerArray)
	{
		if (APongPlayerController * Controller = Cast<APongPlayerController>(PlayerState->GetOwner()))
		{
			if (Controller->GetPawn())
				Controller->GetPawn()->SetActorHiddenInGame(false);
		}
	}
	
	GetWorldTimerManager().SetTimer(SpeedUpTimerHandle, this, &APongGameModeBase::SpeedUp, SpeedUpTimeInterval, true);
}

void APongGameModeBase::ResetMatch(int32 WinnerPlayerId)
{
	MyPongBall->Reset();

	for (APlayerState * PlayerState : GameState->PlayerArray)
	{
		if (APongPlayerState * PongPlayerState = Cast<APongPlayerState>(PlayerState))
		{
			PlayerState->Reset();

			if (APongPlayerController * Controller = Cast<APongPlayerController>(PongPlayerState->GetOwner()))
			{
				Controller->OnPlayerWin(WinnerPlayerId);
				
				if (Controller->GetPawn())
					Controller->GetPawn()->SetActorHiddenInGame(true);
			}
		}
	}

	GetWorldTimerManager().ClearTimer(SpeedUpTimerHandle);
}

void APongGameModeBase::SpeedUp()
{
	// Send speed up event to clients
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		APongPlayerController* PlayerController = Cast<APongPlayerController>(*It);

		if (PlayerController)
		{
			PlayerController->OnSpeedUp();
		}
	}

	MyPongBall->AddSpeed(SpeedUpValue);
}
