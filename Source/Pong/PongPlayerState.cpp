// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#include "PongPlayerState.h"
#include "PongGameState.h"
#include "UI/PongWidgetHUD.h"
#include "Player/PongPlayerController.h"
#include "PongGameModeBase.h"

#include "UnrealNetwork.h"

APongPlayerState::APongPlayerState(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	bIsReady = false;
}

void APongPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APongPlayerState, PlayerScoreID);
	DOREPLIFETIME(APongPlayerState, bIsLeftPlayer);
	DOREPLIFETIME(APongPlayerState, bIsReady);
}

void APongPlayerState::Reset()
{
	Super::Reset();

	bIsReady = false;
	Score = 0;
}

void APongPlayerState::AddScore()
{
	++Score;
}

void APongPlayerState::Server_SetIsReady_Implementation(uint8 bInReady)
{
	bIsReady = bInReady;
	
	AController* OwnerController = Cast<AController>(GetOwner());;
	
	if (APongGameState * PongGameState = GetWorld()->GetGameState<APongGameState>())
	{
		for (APlayerState * PlayerState : PongGameState->PlayerArray)
		{
			if (APongPlayerState * PongPlayerState = Cast<APongPlayerState>(PlayerState))
			{
				if (!PongPlayerState->GetIsReady())
				{
					return;
				}
			}
		}
	}

	if (APongGameModeBase * PongGameModeBase = Cast<APongGameModeBase>(GetWorld()->GetAuthGameMode()))
	{
		PongGameModeBase->BeginMatch();
	}
}
