// Fill out your copyright notice in the Description page of Project Settings.

#include "PongGameState.h"
#include "UnrealNetwork.h"

APongGameState::APongGameState(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	PlayableAreaTopRight = FVector(750.0f, 1000.0f, 0.0f);
	PlayableAreaLowerLeft = FVector(-750.0f, -1000.0f, 0.0f);
}

float APongGameState::GetPlayableAreaWidth() const
{
	return PlayableAreaTopRight.Y - PlayableAreaLowerLeft.Y;
}

float APongGameState::GetPlayableAreaHeight() const
{
	return PlayableAreaTopRight.X - PlayableAreaLowerLeft.X;
}