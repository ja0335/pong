// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#include "PongPaddle.h"
#include "Pong.h"
#include "PongMovementComponent.h"
#include "PongPlayerController.h"

#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
#include "PongGameState.h"

APongPaddle::APongPaddle(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	
	PaddleMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PaddleMeshComponent"));
	PaddleMeshComponent->SetupAttachment(RootComponent);

	// Create an instance of our movement component, and tell it to update the root.
	PongMovementComponent = CreateDefaultSubobject<UPongMovementComponent>(TEXT("PongMovementComponent"));
	PongMovementComponent->UpdatedComponent = RootComponent;

	MoveReplicateDelta = 1.0f / 60.0f;
	MaxReflectionAngle = 70.0f;

	KeyboardMovementSpeed = 250.0f;
	bIsUsingMouse = true;
}

void APongPaddle::BeginPlay()
{
	Super::BeginPlay();
	
	if (Role == ROLE_AutonomousProxy)
	{
		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &APongPaddle::ReplicateMovement, MoveReplicateDelta, true);
	}
}

void APongPaddle::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (GetNetMode() != NM_DedicatedServer)
	{
		FVector NewLocation = GetActorLocation();

		if (bIsUsingMouse)
		{
			if (APongPlayerController * PongPlayerController = Cast<APongPlayerController>(GetController()))
			{
				FHitResult Hit;
				if (PongPlayerController->GetHitResultUnderCursor(ECC_PONG_MOUSE, false, Hit))
				{
					NewLocation.X = Hit.ImpactPoint.X;
				}
			}
		}
		else
		{
			NewLocation.X += AxisValue * KeyboardMovementSpeed * DeltaSeconds;
		}
		
		// Clamp paddle location to play area boundaries
		if (APongGameState * PongGameState = GetWorld()->GetGameState<APongGameState>())
		{
			const float PaddleHalfScale = PaddleMeshComponent->GetComponentScale().X * 0.5f;

			NewLocation.X = FMath::Clamp(
				NewLocation.X,
				PongGameState->GetPlayableAreaLowerLeft().X + PaddleHalfScale,
				PongGameState->GetPlayableAreaTopRight().X - PaddleHalfScale);
		}
		SetActorLocation(NewLocation);
	}
}

void APongPaddle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("MoveUp", IE_Pressed, this, &APongPaddle::MoveUp);
	PlayerInputComponent->BindAction("MoveUp", IE_Released, this, &APongPaddle::MoveReleased);
	PlayerInputComponent->BindAction("MoveDown", IE_Pressed, this, &APongPaddle::MoveDown);
	PlayerInputComponent->BindAction("MoveDown", IE_Released, this, &APongPaddle::MoveReleased);
}

void APongPaddle::MoveUp()
{
	AxisValue = 1.0f;
}

void APongPaddle::MoveDown()
{
	AxisValue = -1.0f;
}

void APongPaddle::MoveReleased()
{
	AxisValue = 0.0f;
}

void APongPaddle::ReplicateMovement()
{
	Server_SetLocation(GetActorLocation());
}

void APongPaddle::Server_SetLocation_Implementation(FVector_NetQuantize NewLocation)
{
	Broadcast_NewLocation(NewLocation);
}

void APongPaddle::Broadcast_NewLocation_Implementation(FVector_NetQuantize NewLocation)
{
	SetActorLocation(NewLocation);
}