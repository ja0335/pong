// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.
#pragma once


#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "PongCamera.generated.h"

UCLASS()
class APongCamera : public ACameraActor
{
	GENERATED_BODY()

public:

	APongCamera(const class FObjectInitializer& ObjectInitializer);
};