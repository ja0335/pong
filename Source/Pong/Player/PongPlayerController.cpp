// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#include "PongPlayerController.h"
#include "PongGameModeBase.h"
#include "PongPaddle.h"
#include "PongCamera.h"
#include "UI/PongWidgetHUD.h"
#include "PongPlayerState.h"

#include "Engine/World.h"
#include "Runtime/Engine/Public/EngineUtils.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"


APongPlayerController::APongPlayerController()
{
	PrimaryActorTick.bCanEverTick = true;
	bShowMouseCursor = true;
}


void APongPlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	MyGameMode = Cast<APongGameModeBase>(GetWorld()->GetAuthGameMode());

	PongCamera = *TActorIterator<APongCamera>(GetWorld());
	ensure(PongCamera);

	if (IsLocalPlayerController() && PongWidgetHUDType != nullptr)
	{
		PongWidgetHUD = CreateWidget<UPongWidgetHUD>(this, PongWidgetHUDType);
		PongWidgetHUD->AddToViewport();
	}
}

void APongPlayerController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
	
	if (APongPlayerState* PongPlayerState = Cast<APongPlayerState>(PlayerState))
	{
		const bool bIsLeftPlayer = InPawn->GetActorLocation().Y < 0.0f;
		PongPlayerState->SetIsLeftPlayer(bIsLeftPlayer);
	}
}

void APongPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	SetViewTarget(PongCamera);
}

void APongPlayerController::SetUseMouse(bool bUseMouse)
{
	if(APongPaddle * PongPaddle = Cast<APongPaddle>(GetPawn()))
		PongPaddle->SetUseMouse(bUseMouse);
}

void APongPlayerController::OnSpeedUp_Implementation()
{
	if (PongWidgetHUD)
		PongWidgetHUD->OnSpeedUp();
}


void APongPlayerController::OnPlayerWin_Implementation(int32 WinnerPlayerId)
{
	if (APongPlayerState* PongPlayerState = Cast<APongPlayerState>(PlayerState))
	{
		if (PongWidgetHUD)
			PongWidgetHUD->OnPlayerWin(WinnerPlayerId == PlayerState->PlayerId);
	}
}