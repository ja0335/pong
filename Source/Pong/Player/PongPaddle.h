// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PongPaddle.generated.h"

class UPongMovementComponent;

UCLASS(Blueprintable)
class APongPaddle : public APawn
{
	GENERATED_BODY()

public:

	APongPaddle(const FObjectInitializer& ObjectInitializer);

	void BeginPlay() override;

	void Tick(float DeltaSeconds) override;

	void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;	
	void MoveUp();
	void MoveDown();
	void MoveReleased();

	UFUNCTION()
	void ReplicateMovement();

	UFUNCTION(Reliable, Server, WithValidation)
	void Server_SetLocation(FVector_NetQuantize NewLocation);
	FORCEINLINE bool Server_SetLocation_Validate(FVector_NetQuantize NewLocation) const { return true; }

	UFUNCTION(Reliable, NetMulticast, WithValidation)
	void Broadcast_NewLocation(FVector_NetQuantize NewLocation);	
	FORCEINLINE bool Broadcast_NewLocation_Validate(FVector_NetQuantize NewLocation) const { return true; }


	FORCEINLINE void SetUseMouse(bool bUseMouse) { bIsUsingMouse = bUseMouse; }

	FORCEINLINE_DEBUGGABLE float GetMaxReflectionAngle() const { return MaxReflectionAngle; }

	FORCEINLINE UStaticMeshComponent * GetPaddleMeshComponent() const { return PaddleMeshComponent; }
protected:
	
	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pong")
	UStaticMeshComponent * PaddleMeshComponent;

	/** our custom movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pong")
	UPongMovementComponent * PongMovementComponent;
	
	/** time delta to replicate movement */
	UPROPERTY(EditDefaultsOnly, Category = "Pong|Networking")
	float MoveReplicateDelta;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pong")
	float KeyboardMovementSpeed;

	/** movement direction (keyboard)*/
	float AxisValue;

	UPROPERTY(EditAnywhere, Category = "Pong")
	float MaxReflectionAngle;
	
	/** if false then its using keyboard */
	uint8 bIsUsingMouse : 1;
};