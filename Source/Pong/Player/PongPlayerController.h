// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PongPlayerController.generated.h"

class APongGameModeBase;
class APongPaddle;
class APongCamera;
class UPongWidgetHUD;

UCLASS()
class APongPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	APongPlayerController();

	FORCEINLINE bool IsLeftPlayer() const { return bIsLeftPlayer == 1; }

	FORCEINLINE void SetIsLeftPLayer(uint8 bInIsLeftPlayer) { bIsLeftPlayer = bInIsLeftPlayer; }
	
	/** instance of HUD */
	FORCEINLINE UPongWidgetHUD * GetPongWidgetHUD() const { return PongWidgetHUD; }

	void SetUseMouse(bool bUseMouse);

	/** Notifies clients about an speed up */
	UFUNCTION(Reliable, Client)
	void OnSpeedUp();

	/** Notifies clients about winner */
	UFUNCTION(Reliable, Client)
	void OnPlayerWin(int32 WinnerPlayerId);

protected:
	void BeginPlay() override;

	void Possess(APawn* InPawn) override;
	
	void Tick(float DeltaSeconds) override;

protected:

	/** cached pointer to GameMode */
	TWeakObjectPtr<APongGameModeBase> MyGameMode;

	/** the type of hud to instance */
	UPROPERTY(Transient, EditDefaultsOnly, Category = "Pong")
	TSubclassOf<UPongWidgetHUD> PongWidgetHUDType;

	/** instance of HUD */
	UPongWidgetHUD * PongWidgetHUD;

	/** the main camera */
	UPROPERTY(Transient)
	APongCamera * PongCamera;
	
	uint8 bIsLeftPlayer : 1;
};