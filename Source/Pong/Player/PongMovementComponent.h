// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.
#pragma once


#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "PongMovementComponent.generated.h"

UCLASS()
class UPongMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()

public:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
};