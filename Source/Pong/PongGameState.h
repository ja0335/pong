// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "PongGameState.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API APongGameState : public AGameState
{
	GENERATED_UCLASS_BODY()
	
public:

	/** the playable area top right */
	FORCEINLINE const FVector & GetPlayableAreaTopRight() const { return PlayableAreaTopRight; }

	/** the playable area lower left */
	FORCEINLINE const FVector & GetPlayableAreaLowerLeft() const { return PlayableAreaLowerLeft; }

	FORCEINLINE float GetPlayableAreaWidth() const;

	FORCEINLINE float GetPlayableAreaHeight() const;
		
protected:
	
	/** the playable area top right */
	UPROPERTY(EditDefaultsOnly, Category = "Pong")
	FVector PlayableAreaTopRight;

	/** the playable area lower left */
	UPROPERTY(EditDefaultsOnly, Category = "Pong")
	FVector PlayableAreaLowerLeft;
};
