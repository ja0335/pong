// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "PongPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class PONG_API APongPlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:	
	APongPlayerState(const FObjectInitializer & ObjectInitializer);


	/** get number of points */
	FORCEINLINE float GetScore() const { return Score; }

	FORCEINLINE void SetPlayerScoreID(int32 InPlayerScoreID) { PlayerScoreID = InPlayerScoreID; }
	FORCEINLINE int32 GetPlayerScoreID() const { return PlayerScoreID; }

	FORCEINLINE void SetIsLeftPlayer(uint8 bInIsLeftPlayer) { bIsLeftPlayer = bInIsLeftPlayer; }
	FORCEINLINE bool GetIsLeftPlayer() const { return bIsLeftPlayer == 1; }

	FORCEINLINE bool GetIsReady() const { return bIsReady == 1; }

	UFUNCTION(Reliable, Server, WithValidation)
	void Server_SetIsReady(uint8 bInReady);
	FORCEINLINE bool Server_SetIsReady_Validate(uint8 bInReady) { return true; }
	
	/** clear scores */
	void Reset() override;
	
	/** player has scored */
	void AddScore();
	
protected:
	UPROPERTY(Transient, Replicated)
	int32 PlayerScoreID;

	UPROPERTY(Transient, Replicated)
	uint8 bIsLeftPlayer : 1;

	UPROPERTY(Transient, Replicated)
	uint8 bIsReady: 1;
};
