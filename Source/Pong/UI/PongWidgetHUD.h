// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#pragma once

#include "Blueprint/UserWidget.h"
#include "PongWidgetHUD.generated.h"

UCLASS()
class UPongWidgetHUD : public UUserWidget
{
	GENERATED_UCLASS_BODY()

public:

	UFUNCTION(BlueprintImplementableEvent, Category = "Pong")
	void OnSpeedUp();

	UFUNCTION(BlueprintImplementableEvent, Category = "Pong")
	void OnPlayerWin(bool bIsWinner);
		
protected:
	
	void NativeConstruct() override;

	void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Pong")
	void OnInputSelected(bool bUseMouse);

	UFUNCTION(BlueprintCallable, Category = "Pong")
	void OnSetReady();

	UFUNCTION(BlueprintImplementableEvent, Category = "Pong")
	void SetIsLeftPlayer(bool bIsLeftPlayer);
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Pong")
	void OponentReady();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Pong")
	void WaitingForOponent();
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Pong")
	void OnBeginMatch();
	
protected:

	/** cached pointer to my GameController */
	TWeakObjectPtr<class APongPlayerController> MyController;
	
	UPROPERTY(BlueprintReadOnly, Category = "Pong")
	int32 Player1Score;

	UPROPERTY(BlueprintReadOnly, Category = "Pong")
	int32 Player2Score;

	/** Input selection, if false then is using keyboard */
	uint8 bUseMouse : 1;
};