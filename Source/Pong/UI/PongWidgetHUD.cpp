// Copyright 2018 Juan Camilo Acosta Arango. All Rights Reserved.

#include "PongWidgetHUD.h"
#include "PongGameState.h"
#include "PongPlayerState.h"
#include "Player/PongPlayerController.h"

UPongWidgetHUD::UPongWidgetHUD(class FObjectInitializer const &ObjectInitializer)
	:Super(ObjectInitializer)
{
}

void UPongWidgetHUD::NativeConstruct()
{
	Super::NativeConstruct();

	SetIsLeftPlayer(GetOwningPlayer()->GetPawn()->GetActorLocation().Y < 0.0f);
}

void UPongWidgetHUD::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	
	if (APongPlayerState * OwningPlayerState = Cast<APongPlayerState>(GetOwningPlayer()->PlayerState))
	{
		if (APongGameState * PongGameState = GetWorld()->GetGameState<APongGameState>())
		{
			for (APlayerState * PlayerState : PongGameState->PlayerArray)
			{
				if (APongPlayerState * PongPlayerState = Cast<APongPlayerState>(PlayerState))
				{
					// Update player scores
					if (PongPlayerState->GetIsLeftPlayer())
					{
						Player1Score = PongPlayerState->Score;
					}
					else
					{
						Player2Score = PongPlayerState->Score;
					}

					if (PongPlayerState != OwningPlayerState)
					{
						if (PongPlayerState->GetIsReady() && !OwningPlayerState->GetIsReady())
							OponentReady();
						else if (!PongPlayerState->GetIsReady() && OwningPlayerState->GetIsReady())
							WaitingForOponent();
						else if (PongPlayerState->GetIsReady() && OwningPlayerState->GetIsReady())
							OnBeginMatch();
					}
				}
			}
		}
	}
}

void UPongWidgetHUD::OnInputSelected(bool bUseMouse)
{
	this->bUseMouse = bUseMouse;


}

void UPongWidgetHUD::OnSetReady()
{	
	if (APongPlayerState * PongPlayerState = Cast<APongPlayerState>(GetOwningPlayer()->PlayerState))
	{
		PongPlayerState->Server_SetIsReady(true);
	}
	
	if (APongPlayerController * PongPlayerController = Cast<APongPlayerController>(GetOwningPlayer()))
	{
		PongPlayerController->SetUseMouse(bUseMouse);
	}
}
